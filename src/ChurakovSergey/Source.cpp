#include "iostream"
#include "fstream"
#include "stdlib.h"
#include "Vector.h"
#include "Matrix.h"
#include "Time.h"
using namespace::std;


int main()
{
	srand(time(NULL));
	Vector a(5);
	a.Init();
	a.Print(cout);
	Vector b(5);
	b.Init();
	b.Print(cout);
	Vector c(1);
	c = a + b;
	c = a + b;
	Vector d;
	d = a - b;
	c.Print(cout);
	d.Print(cout);
	c += a;
	d -= b;
	c.Print(cout);
	d.Print(cout);
	c *= 2;
	d /= 1;
	c.Print(cout);
	d.Print(cout);
	d[1] = 4;
	Matrix aa(2, 2);
	aa.Init();
	aa.Print(cout);
	aa.CreateDiag(5);
	aa.Print(cout);
	aa *= 5;
	aa.Print(cout);
	aa += aa;
	aa.Print(cout);
	aa -= aa;
	aa.Print(cout);
	//cin >> a;
	//cout << a;
	aa.CreateDiag(5);
	aa[1][1]=6;
	cout << aa[1][1];
	Matrix bb, cc;
	bb.ReInit(4, 3);
	cc.ReInit(3, 4);
	bb.Init();
	cc.Init();
	bb.Print(cout);
	cc.Print(cout);
	if (bb == cc)
		cout << "aaa";
	aa = bb*cc;
	aa.Print(cout);
	aa.Transp();
	aa.Print(cout);
	aa.Init();
	bb = aa.GetDiag();
	bb.Print(cout);
	cout << bb.GetDef() << endl;
	Matrix dd;
	dd.CreateDiag(6);
	cout << dd.GetDef() << endl;
	dd *= 2;
	cout << dd.GetDef() << endl;
	dd = dd.GetRevert();
	dd.Print(cout);
	dd.ReInit(2,2);
	dd[0][0] = 1;
	dd[0][1] = 2;
	dd[1][0] = 4;
	dd[1][1] = 5;
	dd.ReInit(3, 3);
	dd[0][0] = 2;
	dd[0][1] = 4;
	dd[1][0] = 5;
	dd[1][1] = -6;
	dd[0][2] = 3;
	dd[1][2] = 1;
	dd[2][0] = 7;
	dd[2][1] = 5;
	dd[2][2] = 4;
	dd.Print(cout);
	cout << dd.GetDef() << endl;
	cout << dd.GetRevert()*91 << endl;
	cout << dd;
	cin >> dd;
	cout << dd;
	ifstream File("Input.txt");
	if (!File.is_open())
	{
		cout << "Error" << endl;
		exit(EXIT_FAILURE);
	}
	Matrix inp(4,4);
	File >> inp;
	File.close();
	ofstream data_out("TextFile2.txt");
	if (!data_out.is_open())
	{
		cout << "Error" << endl;
		exit(EXIT_FAILURE);
	}
	Matrix i, j;
	i = inp;
	data_out << inp << endl;
	data_out << inp.GetDiag() << endl;
	data_out << inp.GetTransp() << endl;
	j = inp.GetRevert();
	data_out << j <<endl;
	data_out << i*j << endl;
	data_out << inp.GetDef() << endl;
	data_out.close();
	return 0;
}