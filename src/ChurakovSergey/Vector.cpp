#include "Vector.h"
#include "Time.h"
#include "stdlib.h"
#include "istream"
#include "ostream"



Vector::Vector()
{
	VSize = 1;
	memo = new Type[1];
	memo[0] = 0;
}

Vector::Vector(int _vsize): VSize(_vsize)
{
	memo = new Type[VSize];
	for (int i = 0; i < VSize; i++)
		memo[i] = 0;
}

Vector::Vector(Vector & vr)
{
	VSize = vr.VSize;
	memo = new Type[VSize];
	for (int i = 0; i < VSize; i++)
		memo[i] = vr.memo[i];
}


Vector::~Vector()
{
	VSize = 0;
	delete[]memo;
	memo = nullptr;
}

void Vector::ReInit(int _vsize)
{
	VSize = _vsize;
	delete[]memo;
	memo = new Type[VSize];
}

void Vector::Init()
{
	int a;
	for (int i = 0; i < VSize; i++)
	{
		a = rand() % 11 - 5;
		memo[i] =(Type)a ;
	}
}

void Vector::Print(ostream & stream)
{
	for (int i = 0; i < VSize; i++)
		stream << memo[i] << " ";
	stream << endl;
}

Vector Vector::operator+(const  Vector & vr)
{
	Vector tmp(VSize);
	if (VSize == vr.VSize)
	{
		tmp.VSize = VSize;
		for (int i = 0; i < VSize; i++)
			tmp.memo[i] = memo[i] + vr.memo[i];
	}
	return tmp;
}

Vector Vector::operator-(const  Vector & vr)
{
	Vector tmp(VSize);
	if (VSize == vr.VSize)
	{
		tmp.VSize = VSize;
		for (int i = 0; i < VSize; i++)
			tmp.memo[i] = memo[i] - vr.memo[i];
	}
	return tmp;
}

Vector Vector::operator*(Type d)
{
	Vector tmp(VSize);
	for (int i = 0; i < VSize; i++)
		tmp.memo[i] = memo[i] * d;
	return tmp;
}

Vector Vector::operator/(Type d)
{
	Vector tmp(VSize);
	try {
		if (d != 0)
			for (int i = 0; i < VSize; i++)
				tmp.memo[i] = memo[i] / d;
		else {
			exep a = DividedByZero;
			throw a;
		}
	}
	catch (exep) {
		cout << "Can't divide by zero";
		exit(EXIT_FAILURE);
	}
	return tmp;
}

Vector & Vector::operator=( Vector & vr)
{
	if (this != &vr)
	{
		ReInit(vr.VSize);
		for (int i = 0;i < VSize;i++)
			memo[i] = vr.memo[i];
	}
	return*this;
}

Vector & Vector::operator+=(Vector & vr)
{
	if (VSize == vr.VSize)
	{
		for (int i = 0;i < VSize;i++)
			memo[i] += vr.memo[i];
	}
	return *this;
}

Vector & Vector::operator-=(Vector & vr)
{
	if (VSize == vr.VSize)
	{
		for (int i = 0;i < VSize;i++)
			memo[i] -= vr.memo[i];
	}
	return *this;
}

Vector & Vector::operator*=(Type d)
{
	for (int i = 0; i < VSize; i++)
		memo[i] *= d;
	return *this;
}

Vector & Vector::operator/=(Type d)
{
	try {
		if (d != 0)
			for (int i = 0; i < VSize; i++)
				memo[i] /= d;
		else
		{
			exep a = DividedByZero;
			throw a;
		}
	}
	catch (exep) {
		cout << "Can't divide by zero";
		exit(EXIT_FAILURE);
	}
	return *this;
}

bool Vector::operator!=(const Vector & vr) const
{
	if (VSize != vr.VSize)
		return true;
	for (int i = 0; i < VSize;i++)
		if (memo[i] != vr.memo[i])
			return true;
	return false;
}

bool Vector::operator==(const Vector & vr) const
{
	if (VSize != vr.VSize)
		return false;
	for (int i = 0; i < VSize;i++)
		if (memo[i] != vr.memo[i])
			return false;
	return true;
}

Type& Vector::operator[](int index)
{
	try {
		if (index >= 0 && index < VSize)
			return memo[index];
		else
		{
			exep a = OutOfRange;
			throw a;
		}
	}
	catch (exep)
	{
		cout << "Index is out of range of this array";
		exit(EXIT_FAILURE);
	}
}

Type Vector::operator[](int index) const
{
	try {
		if (index >= 0 && index < VSize)
			return memo[index];
		else
		{
			exep a = OutOfRange;
			throw a;
		}
	}
	catch (exep)
	{
		cout << "Index is out of range of this array";
		exit(EXIT_FAILURE);
	}
}

void Vector::CreateBase(int _num, int index)
{
	ReInit(_num);
	for (int i = 0; i < _num; i++)
		if (index == i)
			memo[i] = 1;
		else memo[i] = 0;
}

ostream & operator<<(ostream & stream, const Vector & mt)
{
	for (int i = 0; i < mt.VSize; i++)
		stream << mt[i]<<" ";
	stream << endl;
	return stream;
}

istream & operator >> (istream & stream, Vector & mt)
{
	// TODO: �������� ����� �������� return
	for (int i = 0; i < mt.VSize; i++)
		stream >> mt[i];
	return stream;
}
