#include "Matrix.h"
#include "Time.h"
#include "stdlib.h"



Matrix::Matrix()
{
	VecNumber = 1;
	VecSize = 1;
	Vmemo = new Vector[1];
	Vmemo[0].ReInit(1);
}

Matrix::Matrix(int _vnum, int _vsize)
{
	VecNumber = _vnum;
	VecSize = _vsize;
	Vmemo = new Vector[VecNumber];
	for (int i = 0; i < VecNumber; i++)
	{
		Vmemo[i].ReInit(VecSize);
	}
}

Matrix::Matrix(Matrix & mt)
{
	VecNumber = mt.VecNumber;
	VecSize = mt.VecSize;
	Vmemo = new Vector[VecNumber];
	for (int i = 0; i < VecNumber; i++)
		Vmemo[i]=mt.Vmemo[i];
}


Matrix::~Matrix()
{
	delete[]Vmemo;
	VecSize = 0;
	VecNumber = 0;
}

void Matrix::ReInit(int _vnum, int _vsize)
{
	VecNumber = _vnum;
	VecSize = _vsize;
	delete[]Vmemo;
	Vmemo = new Vector[VecNumber];
	for (int i = 0; i < VecNumber;i++)
	{
		Vmemo[i].ReInit(VecSize);
	}
}

void Matrix::Init()
{
	for (int i = 0; i < VecNumber; i++)
		for (int j = 0; j < VecSize;j++)
			Vmemo[i][j] = rand() % 101 - 50;
}

void Matrix::Print(ostream & stream)
{
	stream << "---------------------------"<<endl;
	for (int i = 0; i < VecNumber; i++)
		Vmemo[i].Print(stream);
	stream << "---------------------------"<<endl;
}

void Matrix::CreateDiag(int _vnum)
{
	ReInit(_vnum, _vnum);
	for (int i = 0; i < _vnum; i++)
		Vmemo[i].CreateBase(_vnum, i);
}

Matrix Matrix::operator*(Type d)
{
	Matrix tmp(VecNumber, VecSize);
	for (int i = 0; i < VecNumber; i++)
	{
		tmp.Vmemo[i] = Vmemo[i]*d;
	}
	return tmp;
}

Matrix Matrix::operator/(Type d)
{
	Matrix tmp(VecNumber, VecSize);
	try {
		if (d != 0)
			for (int i = 0; i < VecNumber; i++)
			{
				tmp.Vmemo[i] = Vmemo[i] / d;
			}
		else
		{
			exep a = DividedByZero;
			throw a;
		}
	}
	catch (exep) {
		cout << "Can't divide by zero";
		exit(EXIT_FAILURE);
	}
	return tmp;
}

Matrix Matrix::operator+(const Matrix & mt)
{
	Matrix tmp;
	try {
		if (VecNumber == mt.VecNumber&&VecSize == mt.VecSize)
			for (int i = 0; i < VecNumber; i++)
				tmp.Vmemo[i] = Vmemo[i] + mt.Vmemo[i];
		else
		{
			exep a = WrongMatrixSize;
			throw (a);
		}
	}
	catch (exep)
	{
		cout << "These matrixes don't share the same size";
		exit(EXIT_FAILURE);
	}
	return tmp;
}

Matrix Matrix::operator-(const Matrix & mt)
{
	Matrix tmp;
	try {
	if (VecNumber == mt.VecNumber&&VecSize == mt.VecSize)
		for (int i = 0; i < VecNumber; i++)
			tmp.Vmemo[i] = Vmemo[i] - mt.Vmemo[i];
	else
	{
		exep a = WrongMatrixSize;
		throw (a);
	}
	}
	catch (exep)
	{
		cout << "These matrixes don't share the same size";
		exit(EXIT_FAILURE);
	}
	return tmp;
}

Matrix Matrix::operator=(const Matrix & mt)
{
	if (this != &mt)
	{
		ReInit(mt.VecNumber, mt.VecSize);
		for (int i = 0;i < VecNumber;i++)
			Vmemo[i] = mt.Vmemo[i];
	}
	return*this;
}

Matrix & Matrix::operator+=(const Matrix & mt)
{
	try{
	if (VecNumber == mt.VecNumber&&VecSize == mt.VecSize)
		for (int i = 0; i < VecNumber; i++)
			Vmemo[i] +=  mt.Vmemo[i];
	else
	{
		exep a = WrongMatrixSize;
		throw (a);
	}
	}
	catch (exep)
	{
		cout << "These matrixes don't share the same size";
		exit(EXIT_FAILURE);
	}
	return *this;
}

Matrix & Matrix::operator-=(const Matrix & mt)
{
	try{
	if (VecNumber == mt.VecNumber&&VecSize == mt.VecSize)
		for (int i = 0; i < VecNumber; i++)
			Vmemo[i] -= mt.Vmemo[i];
	else
	{
		exep a = WrongMatrixSize;
		throw (a);
	}
	}
	catch (exep)
	{
		cout << "These matrixes don't share the same size";
		exit(EXIT_FAILURE);
	}
	return *this;
}

Matrix & Matrix::operator*=(Type d)
{
	for (int i = 0; i < VecNumber;i++)
		Vmemo[i] *= d;
	return *this;
}

Matrix & Matrix::operator/=(Type d)
{
	try{
	if (d != 0)
	for (int i = 0; i < VecNumber;i++)
		Vmemo[i] /= d;
	else
	{
		exep a = DividedByZero;
		throw a;
	}
	}
	catch (exep) {
		cout << "Can't divide by zero";
		exit(EXIT_FAILURE);
	}
	return *this;
}

Vector& Matrix::operator[](int index)
{
	try {
		if (index >= 0 && index < VecNumber)
			return Vmemo[index];
		else
		{
			exep a = OutOfRange;
			throw a;
		}
	}
	catch (exep)
	{
		cout << "Index is out of range of this array";
		exit(EXIT_FAILURE);
}
}

Vector Matrix::operator[](int index) const
{
	try {
		if (index >= 0 && index < VecNumber)
			return Vmemo[index];
		else
		{
			exep a = OutOfRange;
			throw a;
		}
	}
	catch (exep)
	{
		cout << "Index is out of range of this array";
		exit(EXIT_FAILURE);
	}
}

Matrix Matrix::operator*(const Matrix & mt)
{
	int a = VecNumber;
	int b = mt.VecSize;
	int c = VecSize;
		Matrix tmp(a, b);
		try {
			if (VecSize == mt.VecNumber)
			{
				for (int i = 0; i < a; i++)
					for (int j = 0; j < b; j++)
					{
						tmp[i][j] = 0;
						for (int k = 0; k < c; k++)
							tmp[i][j] += Vmemo[i][k] * mt[k][j];
					}
			}
			else {
				exep a = WrongMatrixSize;
				throw a;
			}
		}
		catch (exep)
		{
			cout << "These matrixes don't share the same size";
			exit(EXIT_FAILURE);
		}
	return tmp;
}

Matrix & Matrix::operator*=(const Matrix & mt)
{
	try {
	if (VecSize == mt.VecNumber)
	{
		Matrix tmp(VecNumber, mt.VecSize);
		for (int i = 0; i<VecNumber; i++)
			for (int j = 0; j < mt.VecSize; j++)
			{
				tmp[i][j] = 0;
				for (int k = 0; k < VecSize; k++)
					tmp[i][j] += Vmemo[i][k] * mt[k][j];
			}
	*this = tmp;
	}
	else {
		exep a = WrongMatrixSize;
		throw a;
	}
	}
	catch (exep)
	{
		cout << "These matrixes don't share the same size";
		exit(EXIT_FAILURE);
	}
	return *this;
}

bool Matrix::operator!=(const Matrix & mt) const
{
	if (mt.VecSize != VecSize || mt.VecNumber != VecNumber)
		return true;
	for (int i = 0; i < VecNumber; i++)
		if (Vmemo[i] != mt.Vmemo[i])
			return true;
	return false;
}

bool Matrix::operator==(const Matrix & mt) const
{
	if (mt.VecSize != VecSize || mt.VecNumber != VecNumber)
		return false;
	for (int i = 0; i < VecNumber; i++)
		if (Vmemo[i] != mt.Vmemo[i])
			return false;
	return true;
}

Matrix Matrix::GetTransp()
{
	Matrix mt(VecSize, VecNumber);
	for (int i = 0; i < VecNumber; i++)
		for (int j = 0; j < VecSize; j++)
			mt[j][i] = Vmemo[i][j];
	return mt;
}

void Matrix::Transp()
{
	Matrix mt(VecSize, VecNumber);
	for (int i = 0; i < VecNumber; i++)
		for (int j = 0; j < VecSize; j++)
			mt[j][i] = Vmemo[i][j];
	*this = mt;
}

Matrix Matrix::GetDiag()
{
	Matrix tmp = *this;
	try {
		if (VecNumber == VecSize)
		{
			for (int i = 0; i < VecSize - 1;i++)
			{
				for (int j = 1 + i; j < VecNumber; j++)
				{
					if (tmp[j][i] != 0)
					{
						tmp[j] -= tmp[i] * (tmp[j][i] / tmp[i][i]);
					}
				}
			}
			for (int j = 0; j < VecNumber - 1; j++)
				for (int i = j + 1; i < VecSize; i++)
					if (i != j)
						tmp[j][i] = 0;
		}
		else {
			exep a = WrongMatrixSize;
			throw a;
		}
	}
		catch (exep)
		{
			cout << "Matrix must be MxM sized";
			exit(EXIT_FAILURE);
		}

		return tmp;
	}

Matrix Matrix::GetRevert()
{
	Matrix tmp = *this;
	Matrix Rev;
	Rev.CreateDiag(VecNumber);
	if (VecNumber == VecSize)
	{
		for (int i = 0; i < VecSize - 1;i++)
		{
			for (int j = 1 + i; j < VecNumber; j++)
			{
				if (tmp[j][i] != 0)
				{
					Rev[j] -= Rev[i] * (tmp[j][i] / tmp[i][i]);
					tmp[j] -= tmp[i] * (tmp[j][i] / tmp[i][i]);
				}
			}
		}
		for (int j = VecNumber - 2; j >=0 ; j--)
			for (int i =VecSize-1 ; i > j ; i--)
			{
				if (i != j)
				{
					Rev[j] -= Rev[i] * (tmp[j][i] / tmp[i][i]);
					tmp[j][i] = 0;
				}
			}
		for (int i = 0; i < VecNumber; i++)
			Rev[i] /= tmp[i][i];
	}
	return Rev;
}

Type Matrix::GetDef()
{
	Type a;
	try {
		if (VecNumber == VecSize)
		{
			Matrix mt = GetDiag();
			a = 1;
			for (int i = 0; i < VecNumber; i++)
				a *= mt[i][i];
		}
		else {
			exep a = WrongMatrixSize;
			throw a;
		}
	}
	catch (exep)
	{
		cout << "Matrix must be MxM sized";
		exit(EXIT_FAILURE);
	}
	return a;
}

ostream & operator<<(ostream & stream, const Matrix & mt)
{
	stream << "-----------------------------" << endl;
	for (int i = 0; i < mt.VecNumber; i++)
		stream << mt.Vmemo[i];
	stream << "-----------------------------" << endl;
	return stream;
}

istream & operator >> (istream & stream, Matrix & mt)
{
	cout << "Matrix scale: " << mt.VecNumber << "X" << mt.VecSize << endl;
	for (int i = 0; i < mt.VecNumber; i++)
		stream >> mt.Vmemo[i];
	return stream;
}
