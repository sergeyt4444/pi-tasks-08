#pragma once
#include "iostream"
#include "cstdlib"
#include "ostream"
#include "istream"
#include "Vector.h"
using namespace::std;
class Matrix
{
private:
	int VecNumber;
	int VecSize;
	Vector* Vmemo;
public:
	Matrix();
	Matrix(int _vnum, int _vsize);
	Matrix(Matrix& mt);
	~Matrix();
	void ReInit(int _vnum, int _vsize);
	void Init();
	void Print(ostream& stream);
	void CreateDiag(int _vnum);
	Matrix operator*(Type d);
	Matrix operator/(Type d);
	Matrix operator+(const Matrix& mt);
	Matrix operator-(const Matrix& mt);
	Matrix operator=(const Matrix& mt);
	Matrix& operator+=(const Matrix& mt);
	Matrix& operator-=(const Matrix& mt);
	Matrix& operator*=(Type d);
	Matrix& operator/=(Type d);
	Vector& operator[](int index);
	Vector operator[](int index) const;
	Matrix operator*(const Matrix& mt);
	Matrix& operator*=(const Matrix& mt);
	bool operator!=(const Matrix& mt) const;
	bool operator==(const Matrix& mt) const;
	Matrix GetTransp();
	void Transp();
	Matrix GetDiag();
	Matrix GetRevert();
	Type GetDef();
	friend ostream& operator<<(ostream& stream,const Matrix& mt);
	friend istream& operator>>(istream& stream, Matrix& mt);
};

