#pragma once
#include "iostream"
#include "cstdlib"
#include "ostream"
#include "istream"
using namespace::std;
enum exep { OutOfRange, WrongMatrixSize, DividedByZero };
typedef double Type;
class Vector
{
private:
	int VSize;
	Type* memo;
public:
	Vector();
	Vector(int _vsize);
	Vector(Vector& vr);
	~Vector();
	void ReInit(int _vsize);
	void Init();
	void Print(ostream& stream);
	Vector operator+(const Vector& vr);
	Vector operator-(const Vector& vr);
	Vector operator*(Type d);
	Vector operator/(Type d);
	Vector& operator=( Vector&vr);
	Vector& operator+=(Vector&vr);
	Vector& operator-=(Vector&vr);
	Vector& operator*=(Type d);
	Vector& operator/=(Type d);
	bool operator!=(const Vector&vr) const;
	bool operator==(const Vector&vr) const;
	Type& operator[](int index);
	Type operator[](int index) const;
	void CreateBase(int _num, int index);
	friend ostream& operator<<(ostream& stream, const Vector& mt);
//{
//	for (int i = 0; i < mt.VSize; i++)
//		stream << mt.memo[i] << " ";
//	stream << endl;
//	return stream;
//}
	friend istream& operator >> (istream& stream, Vector& mt);
//	{
//		stream >> mt.VSize >> endl;
//		for (int i = 0; i < mt.VSize; i++)
//			stream >> (Type)mt.memo[i]>>endl;
//		return stream;
//	}
};

